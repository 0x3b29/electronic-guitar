# Electronic Guitar

## Description
This is a playground where I tried to design and build my own digital instrument. What came out resembles a guitar but uses buttons instead of touch (e.g. Jammy) or strings like real guitars. It is a prototype. The ergonomics are questionable at best but it is great fun to play with it.

## What do I need to build my own? (Links are NOT sponsored and for reference only. No endorsement)
* 8 x 7cm by 5cm PCB's with 2.54 mm spacing (e.g. https://www.amazon.de/gp/product/B07G88DB6B)
* 138 micro push buttons  (e.g. https://www.amazon.de/gp/product/B08D65M8KP)
* 30 AWG wire (lots of it, maybe 150m) (https://www.amazon.de/gp/product/B08P385FGT)
* 6 ESP32 boards (e.g. https://www.amazon.de/gp/product/B08BZFW41S)
* Male pin headers (e.g. https://www.amazon.de/gp/product/B07DBY753C)
* M4 threaded inserts (e.g. https://www.amazon.de/dp/B08Y597LWL)
* A good soldering station
* Hot glue
* A 5mm steel rod to stiffen the neck of the guitar

## How do I build my own?
* Print all the 3D printed parts
* Glue the neck together using super glue
* Cut a metal rod to the length of the neck and glue it in with some hot glue
* Put to threaded inserts into the body to secure the neck and the backplate to the case with M4 screws
* Start by joining the PCB's together. I used some left over pins to solder them to one big board
* Solder all the buttons to the PCB, using the spacing you like (6 per row)
* Solder all the cables to the pin headers
* Connect all pin headers together using the 5V lines and the GND lines
* Connect the TX and RX pins together, such that the first board's TX line connects to the second board's RX line, an it's TX line to the third board's RX line and so on.
* Use Pull Up resistors on the RX lines if you have problems with noise

## Usage
The instrument plays a lot like a keytar, but with a much different button layout. It can be simply plugged into any PC and used as a MIDI device using the Hairless MIDI<->Serial bridge and the loopMIDI tool.

## Support
Just reach out to me. If I can, I am always eager to help

## Roadmap
There are no big plans for this project. I published it here so that other people can get some code & inspiration for their projects 

## Authors and acknowledgment
* The Idea came from my younger sibling Gérard
* I used a method that I learned from the Switch & Lever’s video “Building a MIDI Controller Using Arduino” (https://www.youtube.com/watch?v=JZ5yPdoPooU&t). In this project, he uses the Hairless MIDI<->Serial Bridge (https://projectgus.github.io/hairless-midiserial/) as well as Tobias Erichsen loopMIDI tool (https://www.tobias-erichsen.de/software/loopmidi.html) to convert bytes from the serial input into valid midi messages.
* All the code, CAD files and anything else are by me, Olivier

## License
Everything is published under the MIT license.

## Project status
This project is concluded. I don't think I will add much more to it. But who knows. Maybe I spend some time learning to play this instrument and need some adjustments.